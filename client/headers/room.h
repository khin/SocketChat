#pragma once

typedef struct{
    int     id;
    char*   name;
} Room;

Room* room_New(int id, char* name);

void room_Add(Room* room);

Room* room_FindByName(char* name);

Room* room_FindById(int id);
