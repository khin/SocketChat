#include <stdlib.h>
#include <string.h>

#include "user.h"

char*   user_nickname	= NULL;
int		room_id			= -1;

void user_SetNickname(char *nickname){
    if (strlen(nickname) < _NICK_MIN_LEN)
        return;

	if (user_nickname)
		free(user_nickname);

    user_nickname = calloc(strlen(nickname), sizeof(char));
	strcpy(user_nickname, nickname);
}

char* user_GetNickname(){
    return user_nickname;
}

void user_SetRoom(int id){
	room_id = id;
}

int user_GetRoom(){
	return room_id;
}
