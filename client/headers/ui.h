#pragma once

/* #############################################################################

	UI Management functions

############################################################################# */

#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>

#include "client-login.h"
#include "client-chat.h"
#include "ui-chat.h"

/* ########################################################################## */

// Initialize variables
void ui_Start(GtkApplication *app, char* builder_resource);

// (Re)Loads UI resource
void ui_LoadResource();

// Get widget from builder
GObject* ui_GetObject(char* widget_name);

/* ########################################################################## */

// Show login window
guint ui_ShowWindowLogin();

// Change login status label text
void ui_LoginStatusChange(char* message);

// Close login window
guint ui_CloseWindowLogin();
