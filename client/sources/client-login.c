#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

#include "client-login.h"
#include "client-chat.h"
#include "client-connection.h"
#include "ui.h"
#include "user.h"

extern pthread_t conn_thread;

void login_AttemptConnection(){
	// Validate data
	GtkEntry *data_entry;
	char* servername;
	char* nickname;
	char msg[64];

	data_entry = (GtkEntry*) ui_GetObject("entry_login_servername");
	servername = (char*) gtk_entry_get_text(data_entry);

	if (strlen(servername) == 0){
		ui_LoginStatusChange("Input server name or IP address");
		return;
	}

	data_entry = (GtkEntry*) ui_GetObject("entry_login_nickname");
	nickname = (char*) gtk_entry_get_text(data_entry);

	if (strlen(nickname) < _NICK_MIN_LEN){
		sprintf(msg, "Nickname must be at least %d characters long", _NICK_MIN_LEN);
		ui_LoginStatusChange(msg);
		return;
	}

	user_SetNickname(nickname);
	ui_LoginStatusChange("Attempting connection to server...");

	//pthread_create(&conn_thread, NULL, conn_AttemptConnection, servername);
	conn_AttemptConnection(servername);
}

void login_ConnectionTimeout(int a){
	ui_LoginStatusChange("Connection timed out");
}

void login_ConnectionFailed(int error){
	printf("Failed to create socket (error %d)\n", error);
	ui_LoginStatusChange("Failed to connect to server");
}

void login_Connected(){
	char* nickname	= user_GetNickname();

	ui_ShowWindowChat(nickname);
	ui_CloseWindowLogin();
}
