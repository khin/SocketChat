#pragma once

#ifndef _NICK_MIN_LEN
#define _NICK_MIN_LEN 3
#endif

void user_SetNickname(char* nickname);

char* user_GetNickname();

void user_SetRoom(int room_id);

int user_GetRoom();
