#pragma once

#include "client-message.h"

#ifndef _CONNECTION_TIMEOUT_
#define _CONNECTION_TIMEOUT_ 5
#endif // _CONNECTION_TIMEOUT_

#ifndef _DEFAULT_SERVER_PORT_
#define _DEFAULT_SERVER_PORT_ 5029
#endif // _DEFAULT_SERVER_PORT_

#ifndef _SERVER_BUFFER_SIZE_
#define _SERVER_BUFFER_SIZE_ 255
#endif // _SERVER_BUFFER_SIZE_

#define STATE_DISCONNECTED  0
#define STATE_WAITBUFFER    1
#define STATE_CLEARTOSEND   2
#define STATE_OKNOROOM      3

void conn_AttemptConnection(char* servername);

void conn_SendInt(int code, int arg);

void conn_SendMessage(int code, char* arg);

void conn_Disconnect();
