#pragma once

#define MSG_CHAR_PARAM	"§"

/* ########################################################################## */

// Possible messages from client

#define MSG_CLT_JOINROOM 0
// Followed by room id

#define MSG_CLT_LEAVEROOM 1
// User is disconnected from the current room

#define MSG_CLT_NICKNAME 2
// Followed by new nickname

#define MSG_CLT_CREATEROOM 3
// Followed by room name

#define MSG_CLT_NEWMSG 4
// Send message to current room (if any)

#define MSG_CLT_DELETEROOM 5
// Remove all users from a room and remove room, argument is room id

#define MSG_CLT_LOGOUT 6
// Disconnect user client

/* ########################################################################## */

// Possible messages from server

#define MSG_SRV_NEWMSG 11
// Sent when a new message is received at the room or the user just joined

#define MSG_SRV_HISTORYOK 12
// Sent when the current room finished sending its buffered messages

#define MSG_SRV_FORCELEAVE 13
// User was removed from the current room (with no previous warning)
// 		- meant to be used when a room is deleted

#define MSG_SRV_NEWROOM 14
// Notify connected users that a new room has been created

#define MSG_SRV_DELETEROOM 15
// Notify client that a room has been deleted

#define MSG_SRV_SERVER_CLOSED 16
// Server closed, client should go back to login screen
