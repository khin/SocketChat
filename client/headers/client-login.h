#pragma once

// Triggered when user clicks connect button
void login_AttemptConnection();

void login_ConnectionTimeout();

void login_ConnectionFailed(int error);

void login_Connected();
