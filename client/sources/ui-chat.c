#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>

#include "client-connection.h"
#include "client-login.h"
#include "client-chat.h"
#include "ui-chat.h"
#include "ui.h"

extern GtkApplication	*app;
extern GtkWindow		*chat_window;

void ui_ShowWindowChat(char* nickname){
	// Load UI data
	if (!chat_window){
		chat_window = (GtkWindow*) ui_GetObject("window_chat");
		gtk_application_add_window(app, chat_window);
	}

	// Connect signals
	g_signal_connect(chat_window, "delete-event",
		G_CALLBACK(ui_OnChatWindowClosed), NULL);

	GtkButton* button = (GtkButton*) ui_GetObject("button_chat_logout");
	g_signal_connect(button, "clicked", G_CALLBACK(chat_Logout), NULL);

	button = (GtkButton*) ui_GetObject("button_chat_create_room");
	g_signal_connect(button, "clicked", G_CALLBACK(ui_CreateRoom), NULL);

	button = (GtkButton*) ui_GetObject("button_chat_change_nickname");
	g_signal_connect(button, "clicked", G_CALLBACK(ui_UpdateNickname), NULL);

	button = (GtkButton*) ui_GetObject("button_chat_send_message");
	g_signal_connect(button, "clicked", G_CALLBACK(ui_OnSendMessage), NULL);

	GtkListBox* listbox = (GtkListBox*) ui_GetObject("listbox_rooms");
	g_signal_connect(listbox, "row-activated", G_CALLBACK(chat_OnChangeRoom), NULL);

	// Display window
	gtk_widget_show_all((GtkWidget*) chat_window);

	// Set initial nickname
	GtkEntry *nickname_entry = (GtkEntry*) ui_GetObject("entry_chat_nickname");
	gtk_entry_set_text(nickname_entry, nickname);
}

void ui_UpdateNickname(){
	GtkEntry *entry = (GtkEntry*) ui_GetObject("entry_chat_nickname");
	char* nickname = (char*) gtk_entry_get_text(entry);

	// Update logic nickname
	chat_ChangeNickname(nickname);
}

void ui_CreateRoom(){
	GtkEntry *entry = (GtkEntry*) ui_GetObject("entry_chat_new_room_name");
	char* name = (char*) gtk_entry_get_text(entry);

	if (strlen(name) == 0) return;

	// Send server a request
	conn_SendMessage(MSG_CLT_CREATEROOM, name);
	gtk_entry_set_text(entry, "");
	gtk_widget_show_all((GtkWidget*) entry);
}

void ui_ClearChatHistory(){
	GtkTextBuffer *buffer = (GtkTextBuffer*) ui_GetObject("textbuffer_chat_history");
	gtk_text_buffer_set_text(buffer, "\0", -1);
	gtk_widget_show_all((GtkWidget*) buffer);
}

void ui_CloseWindowChat(){
	gtk_window_close(chat_window);
	chat_window = NULL;
}

void ui_OnSendMessage(){
	GtkTextBuffer*	buffer = (GtkTextBuffer*) ui_GetObject("textbuffer_chat_message");
	GtkTextIter start_it, end_it;

	gtk_text_buffer_get_bounds(buffer, &start_it, &end_it);

	char* text = gtk_text_buffer_get_text(buffer, &start_it, &end_it, FALSE);

	if (strlen(text) == 0) return;

	conn_SendMessage(MSG_CLT_NEWMSG, text);
	gtk_text_buffer_set_text(buffer, "", -1);
}

gboolean ui_OnChatWindowClosed(GtkWidget* w, GdkEvent* e, gpointer data){
	//if connected with server, disconnect
	conn_Disconnect();

	// Reopen login window
	ui_LoadResource();
	ui_ShowWindowLogin();

	// Tell GTK to destroy the window
	return FALSE;
}

/* ########################################################################## */

void ui_AddRoom(char* name){
	int length = strlen(name);
	if (length == 0) return;

	GtkListBox* room_list = (GtkListBox*) ui_GetObject("listbox_rooms");
	GtkLabel* label = (GtkLabel*) gtk_label_new(name);

	char* buffer = (char*) gtk_label_get_text(label);
	printf("New room created with name '%s'\n", buffer);

	gtk_list_box_insert(room_list, (GtkWidget*) label, -1);
	gtk_widget_show_all((GtkWidget*) room_list);
}

void ui_RemoveRoom(int room_id){
	// Not implemented
}

void ui_NewMessage(char* message){
	int length = strlen(message);
	if (length == 0) return;

    GtkTextBuffer* chat_text = (GtkTextBuffer*) ui_GetObject("textbuffer_chat_history");
	gtk_text_buffer_insert_at_cursor(chat_text, "\n\n", -1);
    gtk_text_buffer_insert_at_cursor(chat_text, message, -1);
}

void ui_SetSendActive(gboolean active){
	GtkWidget* button = (GtkWidget*) ui_GetObject("button_chat_send_message");

	gtk_widget_set_sensitive(button, active);
}

/* ########################################################################## */
