#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>

#include "room.h"

GList* rooms;

Room* room_New(int id, char* name){
    Room* room = malloc(sizeof(Room));

    room->id    = id;
    room->name  = calloc(strlen(name) + 1, sizeof(char));

	strcpy(room->name, name);

    return room;
}

void room_Add(Room* r){
	if (r == NULL){
		printf("Invalis NULL parameter to 'room_Add' function.\n");
		return;
	}

	rooms = g_list_append(rooms, r);
}

Room* room_FindByName(char* name){
	GList* link = rooms;
	Room* room	= NULL;

	printf("Searching for room named '%s'\n", name);

	while (room == NULL && link != NULL){
		GList* next = link->next;
		room = (Room*) link->data;

		printf("\t%s\n", room->name);
		if (strcmp(name, room->name) != 0){
			room = NULL;
			link = next;
		}
		else
			puts("\t\tFound it!");
	}

	return room;
}

Room* room_FindById(int id){
	GList* link = rooms;
	Room* room	= NULL;

	while (room == NULL && link != NULL){
		GList* next = link->next;
		room = (Room*) link->data;

		if (room->id != id){
			room = NULL;
			link = next;
		}
	}

	return room;
}
