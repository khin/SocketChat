#define _POSIX_C_SOURCE		// required for sockaddr_in
#define _GNU_SOURCE			// required for accept4

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <gtk/gtk.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <signal.h>
#include <errno.h>
#include "room.h"
#include "user.h"
#include "server-helper.h"

GList *threads;
GList *users;
GList *rooms;
sem_t rooms_semaphore;

int server_port;
int welcome_socket;
int server_quit;

void ParseArgs(int argc, char* argv[]);
void TerminationHandler(int signum);
void Cleanup();

int main(int argc, char* argv[]){
	rooms	= NULL;
	users	= NULL;
	threads	= NULL;

	sem_init(&rooms_semaphore, 0 , 1);

	server_port		= _DEFAULT_SERVER_PORT_;
	welcome_socket	= -1;

	ParseArgs(argc, argv);
	OnCreateNewRoom("General");

	// Trap SIGINT (Ctrl+C) to finish server appropriately
	struct sigaction sa;
	sa.sa_handler = TerminationHandler;
	sigemptyset(&sa.sa_mask);

	if (sigaction(SIGINT, &sa, NULL) == -1){
		printf("Failed to trap SIGINT (Ctrl+C)\n");
	}

	// Data socket
	int socket	= 0;
	int user_id	= -1;

	struct sockaddr_in client_addr;
	socklen_t client_len = sizeof(client_addr);
	pthread_t *thread;

	InitializeServer(server_port);
	server_quit	= 0;

	while (!server_quit){
		socket = accept4(welcome_socket, (struct sockaddr *) &client_addr, &client_len, SOCK_NONBLOCK);

		if (socket < 0){
			if (errno != EAGAIN && errno != EWOULDBLOCK){
				printf("Failed to accept socket connection (error %d)\n", errno);
			}
			else{
				continue;
			}
		}
		else {
			// Create new user and add it to the list
			User* user = user_New(socket);
			users = g_list_prepend(users, user);

			// Create new thread to handle new connection
			thread = malloc(sizeof(pthread_t));
			pthread_create(thread, NULL, OnNewConnection, &(user->id));

			// Add thread to thread list
			threads = g_list_prepend(threads, thread);
		}
	}

	Cleanup();
	return 0;
}

void ParseArgs(int argc, char* argv[]){
	int i = 1;

	while (i < argc - 1){
		if (argv[i][0] == '-'){
			switch (argv[i][1]){
				case 'p':
					server_port = atoi(argv[++i]);
					if (server_port <= 1024){
						printf("Port %d is reserved. Select a port over 1024.\n", server_port);
						server_port = _DEFAULT_SERVER_PORT_;
					}
					printf("Server port: %d\n", server_port);
				break;
				default: break;
			}
		}

		i++;
	}
}

void TerminationHandler(int signum){
	printf(" <-- SIGINT traped!\n\n");
	server_quit = 1;
}

void Cleanup(){
	printf("Closing pending connections...");
	while(threads != NULL){
		pthread_join(*(pthread_t*)threads->data, NULL);
		threads = g_list_delete_link(threads, threads);
	}
	printf(" done\n");

	printf("Removing user data...\n");
	while(users){
		User* user = (User*)users->data;
		printf("\tid %d - '%s'\n", user->id, user->nickname);

		user_Clean(user);
		users = g_list_delete_link(users, users);
	}
	g_list_free(users);
	printf(" done\n");

	printf("Deleting rooms...\n");
	while(rooms){
		Room* room = (Room*)rooms->data;
		printf("\t'%s'\n", room->name);

		room_Clean(room);
		rooms = g_list_delete_link(rooms, rooms);
	}
	printf("All rooms have been deleted\n");

	close(welcome_socket);
	printf("Server closed.\n");
}
