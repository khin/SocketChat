#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include "user.h"

int UserCount = 0;

User* user_New(int socket){
	User* user = malloc(sizeof(User));

	user->socket	= socket;
	user->connected	= 1;

	user->id		= UserCount++;
	user->room_id	= -1;
	user->nickname	= NULL;

	return user;
}

void user_Clean(User* user){
	if (user->connected)
		close(user->socket);

	if (user->nickname != NULL)
		free(user->nickname);

	user->connected	= 0;
	user->id		= -1;
	user->nickname	= NULL;
}

void user_ChangeNickname(User* user, char* nickname){
	if (nickname == NULL || strlen(nickname) == 0)
		return;

	if (user->nickname != NULL)
		free(user->nickname);

	user->nickname	= calloc(strlen(nickname), sizeof(char));
	strcpy(user->nickname, nickname);
}

void user_EnteredRoom(User* user, int room_id){
	if (!user){
		printf("'user_EnteredRoom': invalid NULL parameter");
		return;
	}

	user->room_id = room_id;
}

User* user_FindByID(GList* list, int id){
	User* user = NULL;

	while (list){
		if (( (User*) list->data)->id == id){
			user = (User*)list->data;
			break;
		}

		list = list->next;
	}

	return user;
}

GList* user_RemoveFromList(GList* list, int user_id){
	int removed	= 0;
	GList* link	= list;

	while (removed == 0 && link != NULL){
		GList* next = link->next;

		if ( ((User*) list->data)->id == user_id){
			list = g_list_delete_link(list, link);
			removed = 1;
			printf("User '%d' removed from room\n", user_id);
		}

		link = next;
	}

	return list;
}

void user_Disconnect(User* user){
	user_EnteredRoom(user, -1);
	user->connected = 0;
}
