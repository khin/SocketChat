#include <stdlib.h>
#include <gtk/gtk.h>

#include "ui.h"
#include "client-login.h"

static void on_activate(GtkApplication *app, gpointer data);

int main(int argc, char* argv[]){
	GtkApplication *app;
	int status = 0;

	// Setting a null application ID allows multiple instances os application
	// 		before, I was using "com.gitlab.khin.SocketChat"
	app = gtk_application_new(NULL, G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(on_activate), NULL);

	status = g_application_run(G_APPLICATION(app), argc, argv);

	g_object_unref(app);
	return status;
}

static void on_activate(GtkApplication *app, gpointer data){
	ui_Start(app, "/ui.glade");
	ui_ShowWindowLogin();
}
