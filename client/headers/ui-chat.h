#pragma once

/* ########################################################################## */

// Show chat window
void ui_ShowWindowChat(char* nickname);

// Update logic nickname with what is in the corresponding text entry
void ui_UpdateNickname();

// Called when the user clicks the 'create room' button
void ui_CreateRoom();

// Close chat window
void ui_CloseWindowChat();

void ui_ClearChatHistory();

void ui_OnSendMessage();

gboolean ui_OnChatWindowClosed(GtkWidget *widget, GdkEvent *e, gpointer data);

/* ########################################################################## */

void ui_AddRoom(char* name);

void ui_RemoveRoom(int room_id);

void ui_NewMessage(char* message);

void ui_SetSendActive(int active);	// 1 is active, 0 is inactive

/* ########################################################################## */
