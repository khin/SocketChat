#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
//#include "client-connection.h"
#include "ui.h"

void chat_Logout();

void chat_ChangeNickname();

void chat_CreateRoom(int id, char* room_name);

void chat_OnChangeRoom(GtkListBox*, GtkListBoxRow*, gpointer);

void chat_EnterRoom(int room_id);

void chat_DeleteRoom(int room_id);

void chat_SendMessage(char* message);
