/* ########################################################################## */

#include <string.h>

#include "client-chat.h"
#include "client-login.h"
#include "client-connection.h"
#include "ui-chat.h"
#include "room.h"
#include "user.h"

/* ########################################################################## */

void chat_Logout(){
	// disconnect from server
	conn_Disconnect();
}

void chat_ChangeNickname(char* nickname){
	if (strlen(nickname) == 0)
		return;

	// Update user data locally and notify server of change
	user_SetNickname(nickname);
	conn_SendMessage(MSG_CLT_NICKNAME, nickname);
}

void chat_OnChangeRoom(GtkListBox* listbox, GtkListBoxRow* row, gpointer data){
	GList* widgets	= gtk_container_get_children((GtkContainer*) row);
	GtkLabel* label	= (GtkLabel*) widgets->data;
	char* name		= (char*) gtk_label_get_text(label);

	if (strlen(name) == 0)
		return;

	Room* old_room = room_FindById(user_GetRoom());
	Room* new_room = room_FindByName(name);

	if (new_room == NULL){
		printf("New room ('%s') not found\n", name);
		return;
	}

	printf("New room id: %d\n", new_room->id);
	if (old_room != NULL){
		printf("\tOld room id: %d\n", old_room->id);
		if (old_room->id == new_room->id) {
			return;
		}
		else {
			user_SetRoom(-1);
			ui_SetSendActive(FALSE);
			conn_SendMessage(MSG_CLT_LEAVEROOM, "");
		}
	}

	user_SetRoom(new_room->id);
	ui_ClearChatHistory();
	conn_SendInt(MSG_CLT_JOINROOM, new_room->id);
}

void chat_CreateRoom(int id, char* name){
	room_Add(room_New(id, name));
	ui_AddRoom(name);
}
