#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <semaphore.h>

#include "room.h"
#include "user.h"
#include "server-helper.h"
#include "server-message.h"

sem_t sem_room_count;
int RoomCount = 0;

Room* room_New(char* name){
	Room* room = malloc(sizeof(Room));
	room->name = calloc(strlen(name) + 1, sizeof(char));
	strcpy(room->name, name);

	if (RoomCount == 0){
		sem_init(&sem_room_count, 0, 1);
	}

	sem_wait(&sem_room_count);
	room->id = RoomCount++;
	sem_post(&sem_room_count);

	room->users		= NULL;
	room->messages	= NULL;

	sem_init(&(room->semaphore), 0, 1);

	return room;
}

void room_Clean(Room* room){
	if (room->name != NULL)
		free(room->name);

	room->id	= -1;
	room->name	= NULL;

	sem_wait(&(room->semaphore));
	g_list_free(room->messages);
	sem_post(&(room->semaphore));
}

void room_AddUser(Room* room, User* user){
	if (!room || !user){
		printf("'room_AddUser': invalid NULL parameter.\n");
		return;
	}

	sem_wait(&(room->semaphore));

	room->users = g_list_append(room->users, user);

	// Send previous messages in the room to new user
	GList* it = room->messages;
	while (it != NULL){
		int sent = 0;
		char* buffer = calloc(_SERVER_BUFFER_SIZE_, sizeof(char));

		sprintf(buffer, "%d%s%s%s", MSG_SRV_NEWMSG, MSG_CHAR_PARAM, ((char*)it->data), MSG_CHAR_PARAM);

		errno = 0;
		sent = send(user->socket, buffer, strlen(buffer), 0);

		free(buffer);

		if (sent <= 0){
			if (errno == EAGAIN || errno == EWOULDBLOCK)
				continue;
			else {
				printf("Failed to send message to user '%s' from room '%s'", user->nickname, room->name);
				printf(" (error %d)\n", errno);
			}
		}

		it = it->next;
	}

	sem_post(&(room->semaphore));

	// Send MSG_SRV_HISTORYOK
	char* buffer	= calloc(5, sizeof(char));
	int length		= sprintf(buffer, "%d", MSG_SRV_HISTORYOK);
	int sent		= 0;

	do {
		errno = 0;	// clear error flag
		sent = send(user->socket, buffer, length, 0);

		if (errno != 0 && errno != EAGAIN && errno != EWOULDBLOCK){
			printf("Failed to send MSG_SRV_HISTORYOK to client id %d\n", user->id);
			break;
		}
	}
	while (sent != length);
}

void room_RemoveUser(Room* room, User* user){
	if (!room){
		printf("'room_RemoveUser': invalid NULL room.\n");
		return;
	}

	if (!user){
		printf("'room_RemoveUser': invalid NULL user.\n");
		return;
	}

	sem_wait(&(room->semaphore));

	printf("\tAttempt to remove user '%s' from room '%s'\n", user->nickname, room->name);
	room->users = g_list_remove(room->users, user);
	//room->users = user_RemoveFromList(room->users, user->id);
	user_EnteredRoom(user, -1);

	sem_post(&(room->semaphore));
}

void room_NewMessage(Room *room, char* message){
	if (!room || !message){
		printf("'room_NewMessage': invalid NULL parameter.\n");
		return;
	}

	sem_wait(&(room->semaphore));

	room->messages = g_list_append(room->messages, message);

	// Send new message to everyone in the room
	char* buffer	= calloc(_SERVER_BUFFER_SIZE_, sizeof(char));
	sprintf(buffer, "%d%s%s%s", MSG_SRV_NEWMSG, MSG_CHAR_PARAM, message, MSG_CHAR_PARAM);
	int length	= strlen(buffer);
	int sent	= 0;

	GList* it = room->users;
	while (it != NULL){
		User* user = ( (User*) it->data );

		errno = 0;
		sent = send(user->socket, buffer, length, 0);

		if (sent <= 0){
			if (errno == EAGAIN || errno == EWOULDBLOCK)
				continue;
			else{
				printf("Failed to send new message to user '%s' (error %d)\n",
					user->nickname, errno);
			}
		}

		it = it->next;
	}

	sem_post(&(room->semaphore));
}

Room* room_FindByID(GList* list, int id){
	Room* room = NULL;
	printf("Looking for room id %d\n", id);

	while (list != NULL){
		if (( (Room*) list->data)->id == id){
			room = (Room*) list->data;
			puts("Found it!");
			break;
		}

		list = list->next;
	}

	return room;
}

int room_GetLastID(){
	int count = 0;

	sem_wait(&sem_room_count);
	count = RoomCount - 1;
	sem_post(&sem_room_count);

	return count;
}
