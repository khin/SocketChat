#pragma once

#ifndef _SERVER_BUFFER_SIZE_
#define _SERVER_BUFFER_SIZE_ 255
#endif

#ifndef _DEFAULT_SERVER_PORT_
#define _DEFAULT_SERVER_PORT_ 5029
#endif // _DEFAULT_SERVER_PORT_

#include <stdlib.h>
#include <stdio.h>

#include "user.h"

void InitializeServer		(int port);
void* OnNewConnection		(void* params);
void OnUserDisconnection	(User* user);

void OnNewMessage		(User* user, char* message);
void OnCreateNewRoom	(char* room_name);
void OnDeleteRoom		(int room_id);
void OnUserEnterRoom	(User* user, int room_id);

void ParseMessage(User* user, char* message);
