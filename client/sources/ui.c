#include <string.h>

#include "ui.h"
#include "client-login.h"
#include "client-connection.h"

/* ########################################################################## */

GtkApplication	*app;
char*			resource;
GtkBuilder		*builder;

GtkWindow		*login_window;
GtkWindow		*chat_window;

/* ########################################################################## */

void ui_Start(GtkApplication *a, char* builder_resource){
	app = a;
	login_window = NULL;
	chat_window  = NULL;

	resource = builder_resource;
	ui_LoadResource();
}

void ui_LoadResource(){
	builder = gtk_builder_new_from_resource(resource);

	if (!builder){
		printf("Failed to load builder\n");
		exit(1);
	}
}

GObject* ui_GetObject(char* name){
	return gtk_builder_get_object(builder, name);
}

/* ########################################################################## */

guint ui_ShowWindowLogin(){
	// Load UI data
	if (!login_window){
		login_window = (GtkWindow*) ui_GetObject("window_login");
		gtk_application_add_window(app, login_window);
	}

	// Connect signals
	GtkButton* button = (GtkButton*) ui_GetObject("button_login_connect");
	g_signal_connect(button, "clicked", G_CALLBACK(login_AttemptConnection), NULL);

	// Display window
	gtk_widget_show_all((GtkWidget*) login_window);

	return FALSE;
}

void ui_LoginStatusChange(char* message){
	GtkLabel* label = (GtkLabel*) ui_GetObject("label_login_status");
	gtk_label_set_text(label, message);
}

guint ui_CloseWindowLogin(){
	gtk_window_close(login_window);
	login_window = NULL;

	return FALSE;
}

/* ########################################################################## */
