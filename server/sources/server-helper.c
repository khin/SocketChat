#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <gtk/gtk.h>
#include <string.h>

#include "server-helper.h"
#include "server-message.h"
#include "room.h"

// Global variables declared elsewhere
extern int welcome_socket;
extern int server_quit;
extern GList* users;
extern GList* rooms;
extern sem_t rooms_semaphore;

void InitializeServer(int server_port){
	welcome_socket = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);

	if (welcome_socket == -1){
		printf("Failed to create socket server (error %d)\n", errno);
		exit(1);
	}

	struct sockaddr_in server_addr;
	server_addr.sin_family		= AF_INET;
	server_addr.sin_addr.s_addr	= htonl(INADDR_ANY);
	server_addr.sin_port		= htons(server_port);

	if (bind(welcome_socket, (struct sockaddr*) &server_addr, sizeof(server_addr)) != 0){
		printf("Failed to bind socket server (error %d)\n", errno);
		exit(1);
	}

	if (listen(welcome_socket, 8) != 0){
		printf("Failed to listen on socket server (error %d)\n", errno);
		exit(1);
	}

	printf("Socket server initialized, listening on port %d...\n", server_port);
}

// Entry point for new client threads
void* OnNewConnection(void* params){
	int user_id		= *(int*)params;
	User* user		= user_FindByID(users, user_id);
	char* buffer	= calloc(_SERVER_BUFFER_SIZE_, sizeof(char));
	int bytes_read	= 0;

	if (user == NULL){
		printf("Error: user not found in user list (User id %d)\n", user_id);
		pthread_exit(0);
	}

	printf("\tNew client joined the server! (id %d)\n", user->id);

    // Send list of existing rooms to the new user
    {
        char* msg   = calloc(_SERVER_BUFFER_SIZE_, sizeof(char));
        int sent	= 0;
        int length  = 0;
        GList* it	= rooms;

		sem_wait(&rooms_semaphore);
        while (it != NULL){
            Room* room = (Room*)it->data;

			memset(msg, 0, _SERVER_BUFFER_SIZE_);
            sprintf(msg, "%d%s%d%s%s%s", MSG_SRV_NEWROOM, MSG_CHAR_PARAM, room->id, MSG_CHAR_PARAM, room->name, MSG_CHAR_PARAM);
            length = strlen(msg);

			printf("Sending message '%s'\n", msg);

            errno = 0;
            sent = send(user->socket, msg, length, 0);

            if (sent == -1){
                if (errno == EAGAIN || errno == EWOULDBLOCK)
                    continue;
                else{
                    printf("Error sending 'MSG_SRV_NEWROOM' (function 'OnNewConnection')");
                    printf(" (error %d)\n", errno);
                }
            }
            else{
                it = it->next;
            }
        }
		sem_post(&rooms_semaphore);
    }

	// Main loop
	while (!server_quit && user->connected){
		memset(buffer, 0, _SERVER_BUFFER_SIZE_);
		bytes_read = read(user->socket, buffer, _SERVER_BUFFER_SIZE_);

		if (bytes_read == -1){
			if (errno == EAGAIN || errno == EWOULDBLOCK)
				// Handle non-blocking socket
				continue;
			else{
				printf("Failed to read socket message (error %d)\n", errno);

				if (errno == ENOTCONN || errno == ESHUTDOWN)
					// Socket is not connected, quit
					user_Disconnect(user);
			}
		}
		else{
			if (strlen(buffer) > 0){
				printf("Id %d sent '%s'\n", user->id, buffer);
				ParseMessage(user, buffer);
			}
		}
	}

	if (server_quit){
		// Send MSG_SRV_SERVER_CLOSED message to this user
		char* close_message = calloc(5, sizeof(char));
		int length = sprintf(close_message, "%d", MSG_SRV_SERVER_CLOSED);
		int sent = 0;

		do{
			errno = 0;
			sent = send(user->socket, close_message, length, 0);

			if (errno != 0 && errno != EAGAIN && errno != EWOULDBLOCK){
				printf("Failed to send MSG_SRV_SERVER_CLOSED to user id %d", user->id);
				printf(" (error %d)\n", errno);
				break;
			}
		} while (sent <= 0);
	}
	else{	// user disconnected
		int removed	= 0;
		GList* link	= users;

		while (removed == 0 && link != NULL){
			GList* next = link->next;

			if ( ((User*)link->data)->id == user->id){
				printf("User disconnected - id %d, '%s'\n", user->id, user->nickname);
				user_Clean(user);
				users = g_list_delete_link(users, link);

				removed = 1;
			}

			link = next;
		}
	}

	free(buffer);
	close(user->socket);
	pthread_exit(0);
}

void ParseMessage(User* user, char* message){
	char* token	= strtok(message, MSG_CHAR_PARAM);

	if (token == NULL) return;

	int msg		= atoi(token);
	int param	= -1;

	switch (msg){
		case MSG_CLT_NEWMSG:
			// Read message data, parameters separated by ":"
			token = strtok(NULL, MSG_CHAR_PARAM);
			if (token != NULL)
				OnNewMessage(user, token);
			break;

		case MSG_CLT_JOINROOM:
			// Read room number
			token	= strtok(NULL, MSG_CHAR_PARAM);

			if (token != NULL){
				param	= atoi(token);
				OnUserEnterRoom(user, param);
			}
			break;

		case MSG_CLT_LEAVEROOM:{
				// Remove user from current room
				Room* room = room_FindByID(rooms, user->room_id);
				room_RemoveUser(room, user);
			}
			break;

		case MSG_CLT_NICKNAME:
			// Read new nickname
			token = strtok(NULL, MSG_CHAR_PARAM);
			if (token != NULL)
				user_ChangeNickname(user, token);
			break;

		case MSG_CLT_CREATEROOM:
			// Read the room name
			token = strtok(NULL, MSG_CHAR_PARAM);
			if (token != NULL)
				OnCreateNewRoom(token);
			break;

		case MSG_CLT_DELETEROOM:
			// Read room id number
			token	= strtok(NULL, MSG_CHAR_PARAM);

			if (token != NULL)
				param	= atoi(token);

			if (param > 0)
				OnDeleteRoom(param);
			break;

		case MSG_CLT_LOGOUT:
			OnUserDisconnection(user);
			break;

		default:
			printf("Message could not be parsed: '%s'\n", message);
			break;
	}
}

void OnNewMessage(User* user, char* message){
	// Build new message as "nickname: message"
	char* new_msg = calloc(
		strlen(user->nickname) + strlen(message) + 2, sizeof(char));

	strcat(new_msg, user->nickname);
	strcat(new_msg, ": ");
	strcat(new_msg, message);

	Room* room = room_FindByID(rooms, user->room_id);
	room_NewMessage(room, new_msg);
}

void OnCreateNewRoom(char* room_name){
    Room* room  = room_New(room_name);

	sem_wait(&(room->semaphore));
	rooms = g_list_append(rooms, room);
	sem_post(&(room->semaphore));

	// Notify users of new room
	char* msg   = calloc(_SERVER_BUFFER_SIZE_, sizeof(char));
	int length  = sprintf(msg, "%d%s%d%s%s%s", MSG_SRV_NEWROOM, MSG_CHAR_PARAM, room->id, MSG_CHAR_PARAM, room->name, MSG_CHAR_PARAM);
    int sent    = 0;

    GList* it   = users;
    while (it != NULL){
        User* user = ( (User*) it->data );

        errno   = 0;
        sent    = send(user->socket, msg, length, 0);

        if (sent <= 0){
            if (errno == EAGAIN || errno == EWOULDBLOCK)
                continue;
            else{
                printf("Error sending new room '%s' to user '%s'", room->name, user->nickname);
                printf(" (error %d)\n", errno);
            }
        }

        it = it->next;
    }
}

void OnDeleteRoom(int room_id){	// not implemented by client
	Room* room = room_FindByID(rooms, room_id);
	if (room == NULL){
		printf("Error: Attempt to delete invalid room (id %d)\n", room_id);
		return;
	}

	// Prepare MSG_SRV_FORCELEAVE
	char* message	= calloc(5, sizeof(char));
	int length		= sprintf(message, "%d", MSG_SRV_FORCELEAVE);

	// Send message to all users in the room
	int sent	= 0;
	GList* it	= room->users;
	while (it != NULL){
		User* user = ( (User*) it->data );

		errno = 0;
		sent = send(user->socket, message, length, 0);

		if (sent == -1){
			if (errno == EAGAIN || errno == EWOULDBLOCK)
				continue;
			else{
				printf("Error sending 'MSG_SRV_FORCELEAVE' (function 'OnDeleteRoom')");
				printf(" (error %d)\n", errno);
			}
		}
		else{
			it = it->next;
		}
	}

	printf("Removing users from room '%s'\n", room->name);
	while (room->users){
		User* user = ( (User*) room->users->data );
		printf("\tNickname '%s'\n", user->nickname);

		room_RemoveUser(room, user);
	}
}

void OnUserDisconnection(User* user){
	if (user->room_id != -1){
		Room* room = room_FindByID(rooms, user->room_id);
		room_RemoveUser(room, user);
	}

	user_Disconnect(user);
}

void OnUserEnterRoom(User* user, int room_id){
	if (room_id >= 0 && room_id <= room_GetLastID()){
		Room* room = room_FindByID(rooms, room_id);
		user_EnteredRoom(user, room_id);
		room_AddUser(room, user);
	}
}
