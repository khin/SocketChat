#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif

#ifndef _SVID_SOURCE
#define _SVID_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <time.h>

#include "client-connection.h"
#include "client-message.h"
#include "client-login.h"
#include "user.h"
#include "room.h"
#include "ui.h"

pthread_t conn_thread;

int _socket     	= 0;
int conn_state		= STATE_DISCONNECTED;
char* in_buffer;

void _conn_ParseServername(struct sockaddr_in *sin, char* servername){
	sin->sin_family	= AF_INET;
	sin->sin_port	= htons(_DEFAULT_SERVER_PORT_);

    if (servername[0] >= '0' && servername[0] <= '9'){
        // Server name is an IP number
		puts("Server name is an IP number");
        sin->sin_addr.s_addr = inet_addr(servername);
    }
    else {
        // Server name is a string
		puts("Server name is a string name");
		struct hostent* server_host = gethostbyname(servername);
		if (server_host != NULL)
        	sin->sin_addr = *((struct in_addr *) server_host->h_addr);
    }

	puts("Server name resolved");
}

void _conn_ParseMessage(char* message){
	char* token	= strtok(message, MSG_CHAR_PARAM);

	while(token != NULL && strlen(token) > 0){
		int code	= -1;
		code		= atoi(token);

		switch (code){
			case MSG_SRV_NEWMSG:
				token = strtok(NULL, MSG_CHAR_PARAM);

				if (token != NULL){
					ui_NewMessage(token);
				}
			break;

			case MSG_SRV_NEWROOM:
				// Read new room id and name
				token = strtok(NULL, MSG_CHAR_PARAM);
				int room_id = atoi(token);

				token = strtok(NULL, MSG_CHAR_PARAM);

				Room* room = room_New(room_id, token);
				room_Add(room);
				ui_AddRoom(room->name);
			break;

			case MSG_SRV_HISTORYOK:
				if (user_GetRoom() != -1){
					conn_state = STATE_CLEARTOSEND;
					ui_SetSendActive(TRUE);
				}
				else
					printf("Error: received 'HISTORYOK' with no room.\n");
			break;

			case MSG_SRV_FORCELEAVE:
				ui_ClearChatHistory();
				ui_SetSendActive(FALSE);
			break;

			case MSG_SRV_SERVER_CLOSED:
				conn_state = STATE_DISCONNECTED;
			break;

			default: break;
		}

		token = strtok(NULL, MSG_CHAR_PARAM);
	}
}

guint _conn_MainLoop(){
    int byte_count	= -1;
	errno = 0;

	if (conn_state == STATE_DISCONNECTED){
		ui_CloseWindowChat();
		free(in_buffer);

		return FALSE;
	}

	memset(in_buffer, 0, _SERVER_BUFFER_SIZE_);
	byte_count = read(_socket, in_buffer, _SERVER_BUFFER_SIZE_);

	if (byte_count == -1){
		if (errno != EAGAIN && errno != EWOULDBLOCK){
			printf("Failed to read socket message (error %d)\n", errno);

			if (errno == ENOTCONN || errno == ESHUTDOWN){
				// Socket is not connected, quit
				conn_state = STATE_DISCONNECTED;
			}
		}
	}
	else{
		printf("Message received: '%s'\n", in_buffer);
		_conn_ParseMessage(in_buffer);
	}

	return TRUE;
}

void conn_AttemptConnection(char* servername){
	//char* servername = (char*) param;
    struct sockaddr_in s_srv;

	_socket = -1;
    _socket = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);

    if (_socket == -1){
        login_ConnectionFailed(errno);
        pthread_exit(NULL);
    }

	printf("Socket created OK\n");
    _conn_ParseServername(&s_srv, servername);

    // Control timeout of non-blocking socket
    errno = 0;
    time_t start_time   = time(NULL);
    time_t timer        = time(NULL);
    int is_connected    = 0;

    do {
        switch (errno){
        case 0:
        case EINPROGRESS:
        case EALREADY:
            if (connect(_socket, (struct sockaddr*)&s_srv, sizeof(s_srv)) == 0)
                is_connected = 1;
            break;
        case EISCONN:
            is_connected = 1;
            break;
        default:
            printf("Error connecting to server (error %d)\n", errno);
            is_connected = -1;
            break;
        }

        timer = time(NULL);
    } while (is_connected == 0 && difftime(timer, start_time) < _CONNECTION_TIMEOUT_);

	if (is_connected == 0){
        // Timeout
        login_ConnectionTimeout();
		return;
	}
	else if (is_connected == -1){
        // Error
        login_ConnectionFailed(errno);
		return;
	}

    // Open chat window
    login_Connected();

	// Send nickname to server
	char* nickname = user_GetNickname();
	conn_SendMessage(MSG_CLT_NICKNAME, nickname);

	// Initialize buffers and update connection state
	conn_state	= STATE_OKNOROOM;
	in_buffer	= calloc(_SERVER_BUFFER_SIZE_, sizeof(char));

	// Register main loop
	gdk_threads_add_idle((GSourceFunc) _conn_MainLoop, NULL);
}

void conn_SendMessage(int code, char* arg){
	char* message = NULL;

	if (strlen(arg) == 0){
		message = calloc(3, sizeof(char));
		sprintf(message, "%d", code);
	}
	else{
		message = calloc(strlen(arg) + 5, sizeof(char));
		sprintf(message, "%d%s%s", code, MSG_CHAR_PARAM, arg);
	}

	printf("Sending: %s\n", message);

	int length	= strlen(message);
	int sent	= 0;

	do {
		errno = 0;
		sent = send(_socket, message, length, 0);

		if (sent == -1){
			if (errno == EAGAIN || errno == EWOULDBLOCK)
				continue;
			else{
				printf("Failed sending message to server (error %d)\n", errno);
				printf("\tMessage: '%s'\n", message);
				break;
			}
		}
	} while (sent != length);
}

void conn_SendInt(int code, int arg){
	char* msg = calloc(5, sizeof(char));
	sprintf(msg, "%d", arg);

	conn_SendMessage(code, msg);
	free(msg);
}

void conn_Disconnect(){
	conn_SendMessage(MSG_CLT_LOGOUT, "");

	conn_state = STATE_DISCONNECTED;
}
