#pragma once

/* #############################################################################

	Room data

############################################################################# */

#include <gtk/gtk.h>
#include <semaphore.h>
#include "user.h"

/* ########################################################################## */

typedef struct{
	int		id;
	char	*name;
	GList	*users;
	GList	*messages;
	sem_t	semaphore;
} Room;

/* ########################################################################## */

Room* room_New(char* name);

void room_Clean(Room* room);

void room_AddUser(Room*, User*);

void room_RemoveUser(Room*, User*);

void room_NewMessage(Room*, char* message);

Room* room_FindByID(GList*, int id);

int room_GetLastID();
