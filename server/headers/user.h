#pragma once

/* #############################################################################

	User data

############################################################################# */

typedef struct{
	int 	socket;
	int		connected;

	int		id;
	char	*nickname;	// user nickname
	int		room_id;	// current room (or -1 if user just logged in)
} User;

/* ########################################################################## */

User* user_New(int socket);

void user_Clean(User* user);

void user_ChangeNickname(User*, char* new_nickname);

void user_EnteredRoom(User*, int room_id);

User* user_FindByID(GList*, int user_id);

GList* user_RemoveFromList(GList*, int user_id);

void user_Disconnect(User* user);
